'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.on('/').render('welcome')
Route.get('login-example', 'ExampleController.loginExample')


Route.group(() => {

  Route.get('rule-types', 'RuleTypeController.index')
  Route.get('action-types', 'ActionTypeController.index')

  Route.get('sites/:siteId/content-types/positions-score', 'PositionsScoreController.index')
  Route.get('sites/:siteId/content-types/:contentTypeId/positions-score', 'PositionsScoreController.show')
  Route.post('sites/:siteId/content-types/:contentTypeId/positions-score', 'PositionsScoreController.store')
  Route.delete('sites/:siteId/content-types/:contentTypeId/positions-score', 'PositionsScoreController.destroy')

  Route.get('sites/:siteId/content-types', 'SiteController.getContentTypes')
  Route.get('sites/:siteId/content-types/:contentTypeId', 'SiteController.getBrands')
  Route.get('sites/:siteId/ribbons', 'SiteController.getRibbons')
  Route.post('sites/:siteId/toggle-brand/:brandId/type/:type', 'SiteController.toggleBrand')

  Route.resource('sites/:siteId/lists', 'ListController')

  Route.get('sites/:siteId/list-placements/:id/get-with-experiment', 'ListPlacementController.getWithExperiment')
  Route.get('sites/:siteId/list-placements/get-list-from-params', 'ListPlacementController.getListFromParams')
  Route.resource('sites/:siteId/list-placements', 'ListPlacementController')

  Route.get ('sites/:siteId/list-placements/:lpId/experiments/get-active', 'ExperimentController.getActive')
  Route.post('sites/:siteId/list-placements/:lpId/experiments/:id/clone', 'ExperimentController.clone')
  Route.post('sites/:siteId/list-placements/:lpId/experiments/:id/stop', 'ExperimentController.stop')
  Route.post('sites/:siteId/list-placements/:lpId/experiments/:id/activate', 'ExperimentController.activate')
  Route.resource('sites/:siteId/list-placements/:lpId/experiments', 'ExperimentController')

  // TODO NEED TO rewrite this
  Route.get('external/sites/:siteId/lists/:listId', 'SiteController.getList')
  Route.get('external/sites/:siteId/lists', 'SiteController.getLists')
  Route.get('external/sites', 'SiteController.getSites')

  Route.get('external/sites/:id', 'SiteController.getSite')

})
.prefix('api/v1/')
.formats(['json'], true) // all urls needs to have .json extension
.middleware('tpauth')
