class UrlBuilder {
  static experiments (siteId, lpId, experimentId, action) {
    let url = 'api/v1/sites/' + siteId + '/list-placements/' + lpId + '/experiments'
    url += (experimentId ? '/' + experimentId : '')
    url += (action ? '/' + action : '') + '.json'

    return url
  }

  static listPlacement (siteId, listPlacementId, action) {
    let url = 'api/v1/sites/' + siteId + '/list-placements'
    url += (listPlacementId ? '/' + listPlacementId : '')
    url += (action ? '/' + action : '') + '.json'

    return url
  }

  static list (siteId, listId) {
    let url = 'api/v1/sites/' + siteId + '/lists'
    url += (listId ? '/' + listId : '') + '.json'

    return url
  }

  static positionsScore (siteId, contentTypeId) {
    let url = 'api/v1/sites/' + siteId + '/content-types'
    url += (contentTypeId ? '/' + contentTypeId : '') 
    url += '/positions-score.json'

    return url
  }
}

module.exports = UrlBuilder
