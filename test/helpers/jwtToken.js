const jwt = require('jsonwebtoken')
const Config = use('Config')

let _token = null

class Token {
  static async get (email) {
    if(!_token) {
     _token = await this.create(email)
    }
    return _token
  }

  static async getNew (email) {
    let token = await this.create(email)
    return token
  }

  static async create (email) {
    email = email || "barak@trafficpoint.io" 
    let token = await this._signToken(email)
    return token
  }

  static _signToken (email) {
    let secret = Config.get('app.appKey')
    let options = {}

    return new Promise((resolve, reject) => {
      let expirationTime = Math.floor(Date.now() / 1000) + (60 * 60)
      jwt.sign({
        sub: email,
        exp: expirationTime
      }, secret, options, function (error, token) {
        if (error) {
          return reject(error)
        }
        resolve(token)
      })
    })
  }
}

module.exports = Token
