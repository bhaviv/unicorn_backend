const UrlBuilder =  require('../urlBuilder')
const DisplayResponseErrors = require('../displayError')

let listPlacementObject = {
  siteId: 1,
  pageId: 1,
  extra: 'extra-text',
  listId: 1
}

const buildListPlacement = async (client, jwtToken, listPlacement, doNotDisplayErrors) => {
  listPlacement = Object.assign({}, listPlacementObject, listPlacement ? listPlacement: {}) 

  let createUrl = UrlBuilder.listPlacement(1)
  const response = await client
    .post(createUrl)
    .header('jwt_token', jwtToken)
    .send(listPlacement)
    .end()

  if (!doNotDisplayErrors) {
    DisplayResponseErrors(response)
  }

  return {response, listPlacement}
}

module.exports = buildListPlacement 
