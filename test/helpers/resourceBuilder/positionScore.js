const UrlBuilder =  require('../urlBuilder')
const DisplayResponseErrors = require('../displayError')


const BuildNewPositionsScore = async (client, jwtToken, siteId, contentTypeId) => {
  let PositionsScore = [
    {
      score: 9.5,
      stars: 4.5,
      scoreText: 'very very good'
    },
    {
      score: 9,
      stars: 4.2,
      scoreText: 'very good'
    },
    {
      score: 7.8,
      stars: 4.1,
      scoreText: ' good'
    }
  ]

  let storeUrl = UrlBuilder.positionsScore(siteId, contentTypeId)

  const response = await client
    .post(storeUrl)
    .header('jwt_token', jwtToken)
    .send(PositionsScore)
    .end()

  DisplayResponseErrors(response)

  return {response, PositionsScore}
}

module.exports = BuildNewPositionsScore 
