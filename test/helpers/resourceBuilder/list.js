const _ = require('lodash')
const UrlBuilder =  require('../urlBuilder')
const DisplayResponseErrors = require('../displayError')

let postsToSave = [
  {
    id: 141,
    ribbonId: 2,
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  },
  {
    id: 139,
    ribbonId: null,
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  },
  {
    id: 137,
    ribbonId: null,
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  }
]

let listObject = {
  siteId: 1, 
  name: "aaaa", 
  description: 'some description',
  contentTypeId: 1, 
  type: "manual", 
  isMobileSpecific: false, 
  desktop: {
    brands: postsToSave,
    notUsedBrands: []
  },
  mobile: {
    brands: postsToSave,
    notUsedBrands: []
  }
}


const buildList = async (client, jwtToken, list, doNotDisplayErrors) => {
  list = _.merge({}, listObject, list ? list: {}) 

  let createUrl = UrlBuilder.list(1)
  const response = await client
    .post(createUrl)
    .header('jwt_token', jwtToken)
    .send(list)
    .end()

  if (!doNotDisplayErrors) {
    DisplayResponseErrors(response)
  }

  return {response, list}
}

module.exports = buildList 
