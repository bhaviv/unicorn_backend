module.exports = {
  List: require('./list'),
  ListPlacement: require('./listPlacement'),
  PositionsScore: require('./positionScore')
}
