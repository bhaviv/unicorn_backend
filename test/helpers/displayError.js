const displayResponseErrors = (response) => {
  if (response.error) {
    console.error(response.error)
  }
}

module.exports = displayResponseErrors
