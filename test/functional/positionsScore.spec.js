'use strict'

const DisplayResponseError =  require('../helpers/displayError')
const UrlBuilder =  require('../helpers/urlBuilder')
const Token =  require('../helpers/jwtToken')
const ResourceBuilder =  require('../helpers/resourceBuilder')

const Database = use('Database')
const { test, trait, before, beforeEach } = use('Test/Suite')('List')
 
trait('Test/ApiClient')

let jwtToken

before(async () => {
  jwtToken = await Token.get()
})

beforeEach(async () => {
  await Database.table('positions_scores').delete()
})

let siteId = 1
let contentTypeId = 1

test('can create new PositionsScore ', async ({ client, assert }) => {
  let {response, PositionsScore} = await ResourceBuilder.PositionsScore(client, jwtToken, siteId, contentTypeId)
  response.assertStatus(200)

  let recievedPositionsScore = response.body.data
  assert.isArray(recievedPositionsScore)
  assert.equal(recievedPositionsScore.length, PositionsScore.length)
  assert.exists(recievedPositionsScore[0].id)
  assert.equal(recievedPositionsScore[0].siteId, siteId)
  assert.equal(recievedPositionsScore[0].contentTypeId, contentTypeId)
  assert.equal(recievedPositionsScore[0].position, 1)
  assert.equal(recievedPositionsScore[0].score, PositionsScore[0].score)
  assert.equal(recievedPositionsScore[0].stars, PositionsScore[0].stars)
  assert.equal(recievedPositionsScore[0].scoreText, PositionsScore[0].scoreText)
})

test('get one PositionsScore', async ({ client, assert }) => {
  let {response, PositionsScore} = await ResourceBuilder.PositionsScore(client, jwtToken, siteId, contentTypeId)
  response.assertStatus(200)

  let getPositionsScoreUrl = UrlBuilder.positionsScore(siteId, contentTypeId)

  const getPositionsScoreResponse = await client
    .get(getPositionsScoreUrl)
    .header('jwt_token', jwtToken)
    .end()

  getPositionsScoreResponse.assertStatus(200)
  let recievedPositionsScore = getPositionsScoreResponse.body.data
  assert.isArray(recievedPositionsScore)
  assert.equal(recievedPositionsScore.length, PositionsScore.length)
  assert.exists(recievedPositionsScore[0].id)
})


