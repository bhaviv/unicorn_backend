'use strict'

const DisplayResponseErrors =  require('../helpers/displayError')
const UrlBuilder =  require('../helpers/urlBuilder')
const Token =  require('../helpers/jwtToken')
const ResourceBuilder =  require('../helpers/resourceBuilder')
const RibbonRepo = make('App/Repositories/Ribbon')

const Database = use('Database')
const { test, trait, before, beforeEach } = use('Test/Suite')('List')
 
trait('Test/ApiClient')

let jwtToken
let API_USER='SYSTEM_API'

before(async () => {
  jwtToken = await Token.get()
})

beforeEach(async () => {
  await Database.table('lists').delete()
})

test('can create new list of type review ', async ({ client, assert }) => {
  let listObject = {
    type: 'review'
  }
  let {response, list} = await ResourceBuilder.List(client, jwtToken, listObject)
  response.assertStatus(200)

  let recievedList = response.body.data
  assert.exists(recievedList.id)
  assert.equal(recievedList.name, list.name)
  assert.equal(recievedList.type, listObject.type)
  let brand1 = recievedList.desktop.brands[0]
  assert.notExists(brand1.score)
  assert.notExists(brand1.stars)
  assert.notExists(brand1.scoreText)
})

test('can create new list of type position ', async ({ client, assert }) => {
  let listObject = {
    type: 'position'
  }
  let {response, list} = await ResourceBuilder.List(client, jwtToken, listObject)
  response.assertStatus(200)
  let recievedList = response.body.data
  assert.exists(recievedList.id)
  assert.equal(recievedList.name, list.name)
  assert.equal(recievedList.type, listObject.type)
  let brand1 = recievedList.desktop.brands[0]
  assert.notExists(brand1.score)
  assert.notExists(brand1.stars)
  assert.notExists(brand1.scoreText)
})

test('can create new list of type manual ', async ({ client, assert }) => {
  let listObject = {
    type: 'manual'
  }
  let {response, list} = await ResourceBuilder.List(client, jwtToken, listObject)
  response.assertStatus(200)
  let recievedList = response.body.data
  assert.exists(recievedList.id)
  assert.equal(recievedList.name, list.name)
  assert.equal(recievedList.type, listObject.type)
  let brand1 = recievedList.desktop.brands[0]
  assert.exists(brand1.score)
  assert.exists(brand1.stars)
  assert.exists(brand1.scoreText)
})

test('can update list ', async ({ client, assert }) => {
  let {response, list} = await ResourceBuilder.List(client, jwtToken)

  let createdList = response.body.data
  createdList.name = "updated name"
  let updateUrl = UrlBuilder.list(createdList.siteId, createdList.id)

  const updateResponse = await client
    .put(updateUrl)
    .header('jwt_token', jwtToken)
    .send(createdList)
    .end()

  DisplayResponseErrors(updateResponse)
  updateResponse.assertStatus(200)
  let updatedList = updateResponse.body.data
  assert.exists(updatedList.id)
  assert.equal(updatedList.name, createdList.name)
})

test('get all lists for a site ', async ({ client, assert }) => {
  let l1 = await ResourceBuilder.List(client, jwtToken)
  let l2 = await ResourceBuilder.List(client, jwtToken)

  let getAllUrl = UrlBuilder.list(l1.list.siteId)
  const getListsResponse = await client
    .get(getAllUrl)
    .header('jwt_token', jwtToken)
    .end()

  getListsResponse.assertStatus(200)
  let data = getListsResponse.body.data
  assert.isArray(data)
  assert.equal(data.length, 2)
  assert.equal(data[0].name, l1.list.name)
})

test('It can get lists for site by ContentType slug ', async ({ client, assert }) => {
  const ContentTypeRepo = make('App/Repositories/ContentType')
  let contentTypes = await ContentTypeRepo.all()
  contentTypes = contentTypes.toJSON()
  let contentTypeId1 = contentTypes[0].id
  let contentTypeSlug = contentTypes[0].slug
  let contentTypeId2 = contentTypes[1].id
  let listObject = {
    contentTypeId: contentTypeId1
  }
  let l1 = await ResourceBuilder.List(client, jwtToken, listObject)
  listObject = {
    contentTypeId: contentTypeId2 
  }
  let l2 = await ResourceBuilder.List(client, jwtToken, listObject)

  let getAllUrl = UrlBuilder.list(l1.list.siteId)
  getAllUrl += '?contentTypeSlug=' + contentTypeSlug
  const getListsResponse = await client
    .get(getAllUrl)
    .header('jwt_token', jwtToken)
    .end()

  DisplayResponseErrors(getListsResponse)

  getListsResponse.assertStatus(200)
  let data = getListsResponse.body.data
  assert.isArray(data)
  assert.equal(data.length, 1)
  assert.equal(data[0].name, l1.list.name)
})

test('It get one list', async ({ client, assert }) => {
  let l1 = await ResourceBuilder.List(client, jwtToken)

  let getListUrl = UrlBuilder.list(l1.response.body.data.siteId, l1.response.body.data.id)

  const getListResponse = await client
    .get(getListUrl)
    .header('jwt_token', jwtToken)
    .end()

  DisplayResponseErrors(getListResponse)

  getListResponse.assertStatus(200)
  let data = getListResponse.body.data
  assert.exists(data.id)
  assert.equal(data.name, l1.list.name)
})

test('It get list for site with forSite param', async ({ client, assert }) => {
  let siteId = 1
  let listObject = {
    type: 'position',
  }
  let l1 = await ResourceBuilder.List(client, jwtToken, listObject)
  let createdList = l1.response.body.data
  let getListUrl = UrlBuilder.list(createdList.siteId, createdList.id)
  getListUrl +="?forSite=true"
  let ps = await ResourceBuilder.PositionsScore(client, jwtToken, siteId, createdList.contentTypeId)
  ps.response.assertStatus(200)

  const getListResponse = await client
    .get(getListUrl)
    .header('jwt_token', jwtToken)
    .end()

  DisplayResponseErrors(getListResponse)

  getListResponse.assertStatus(200)
  let listData = getListResponse.body.data
  assert.exists(listData.id)
  assert.equal(listData.name, l1.list.name)
  let brands = listData.desktop.brands
  assert.equal(brands[0].score, ps.PositionsScore[0].score)
  assert.equal(brands[0].scoreText, ps.PositionsScore[0].scoreText)
  assert.equal(brands[0].stars, ps.PositionsScore[0].stars)
})

test('It get list for site with ribbon', async ({ client, assert }) => {
  let siteId = 1
  let ribbons = await RibbonRepo.getBySite(siteId)
  let listObject = {
    type: 'position',
    desktop: {
      brands: [
        {
          ribbonId: ribbons[0].id
        }
      ]
    }
  }
  let l1 = await ResourceBuilder.List(client, jwtToken, listObject)
  let createdList = l1.response.body.data
  let getListUrl = UrlBuilder.list(createdList.siteId, createdList.id)
  getListUrl +="?forSite=true"

  const getListResponse = await client
    .get(getListUrl)
    .header('jwt_token', jwtToken)
    .end()

  DisplayResponseErrors(getListResponse)

  let listData = getListResponse.body.data
  let brands = listData.desktop.brands
  assert.equal(brands[0].ribbonName, ribbons[0].name)
})


test('can destroy list ', async ({ client, assert }) => {
  let {response, list} = await ResourceBuilder.List(client, jwtToken)

  let createdList = response.body.data

  let deleteUrl = UrlBuilder.list(1, createdList.id)
  const deleteResponse = await client
    .delete(deleteUrl)
    .header('jwt_token', jwtToken)
    .end()

  DisplayResponseErrors(deleteResponse)
  deleteResponse.assertStatus(200)
  let deleteResult = deleteResponse.body.data
  assert.isTrue(deleteResult.res)
})

test('cannot destroy un existed list ', async ({ client, assert }) => {
  let {response, list} = await ResourceBuilder.List(client, jwtToken)

  let dummyNumber = 12345 

  let deleteUrl = UrlBuilder.list(1,dummyNumber)
  const deleteResponse = await client
    .delete(deleteUrl)
    .header('jwt_token', jwtToken)
    .end()

  deleteResponse.assertStatus(500)
})


test('It Cannot create list with with unknown user', async ({ client, assert }) => {
  let localJwtToken = await Token.getNew('DUMY_USER_EMAIL')
  let l1 = await ResourceBuilder.List(client, localJwtToken, null, true)
  l1.response.assertStatus(403)
})


test('It Cannot get list with with unknown user', async ({ client, assert }) => {
  let l1 = await ResourceBuilder.List(client, jwtToken, null, true)
  l1.response.assertStatus(200)

  let localJwtToken = await Token.getNew('DUMY_USER_EMAIL')
  let getListUrl = UrlBuilder.list(l1.response.body.data.siteId, l1.response.body.data.id)

  const getListResponse = await client
    .get(getListUrl)
    .header('jwt_token', localJwtToken)
    .end()

  getListResponse.assertStatus(403)
})

test('It Can get list with with API user - test permissions', async ({ client, assert }) => {
  let l1 = await ResourceBuilder.List(client, jwtToken, null, true)
  l1.response.assertStatus(200)

  let localJwtToken = await Token.getNew(API_USER)
  let getListUrl = UrlBuilder.list(l1.response.body.data.siteId, l1.response.body.data.id)

  const getListResponse = await client
    .get(getListUrl)
    .header('jwt_token', localJwtToken)
    .end()

  DisplayResponseErrors(getListResponse)

  getListResponse.assertStatus(200)
})

