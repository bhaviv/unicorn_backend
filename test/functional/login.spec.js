'use strict'

const displayResponseErrors =  require('../helpers/displayError')
const Token =  require('../helpers/jwtToken')

const { test, trait } = use('Test/Suite')('Login')
 
trait('Test/ApiClient')


test('it cannot login without token ', async ({ client, assert }) => {
  let url = 'api/v1/action-types.json'
  let token = 'none'
  const response = await client
    .get(url)
    .header('jwt_token', token)
    .end()

  response.assertStatus(401)
})


test('it can login ', async ({ client, assert }) => {
  let jwtToken = await Token.get()
  let url = 'api/v1/action-types.json'
  const response = await client
    .get(url)
    .header('jwt_token', jwtToken)
    .end()


  response.assertStatus(200)
})


