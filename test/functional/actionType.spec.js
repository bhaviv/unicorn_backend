'use strict'

const DisplayResponseErrors =  require('../helpers/displayError')
const UrlBuilder =  require('../helpers/urlBuilder')
const Token =  require('../helpers/jwtToken')
const ResourceBuilder =  require('../helpers/resourceBuilder')

const { test, trait, before} = use('Test/Suite')('List')
 
trait('Test/ApiClient')

let jwtToken

before(async () => {
  jwtToken = await Token.get()
})

test('get all actionTypes for a site ', async ({ client, assert }) => {
  let getAllUrl = 'api/v1/action-types.json'
  const getActionTypesResponse = await client
    .get(getAllUrl)
    .header('jwt_token', jwtToken)
    .end()

  DisplayResponseErrors(getActionTypesResponse)
  getActionTypesResponse.assertStatus(200)
  let data = getActionTypesResponse.body.data
  assert.isArray(data)
  assert.equal(data.length, 2)
})

