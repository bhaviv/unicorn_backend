'use strict'

const DisplayResponseError =  require('../helpers/displayError')
const UrlBuilder =  require('../helpers/urlBuilder')
const Token =  require('../helpers/jwtToken')
const ResourceBuilder =  require('../helpers/resourceBuilder')

const Database = use('Database')
const { test, trait, before, beforeEach } = use('Test/Suite')('List Placement')
 
trait('Test/ApiClient')

let jwtToken
let API_USER='SYSTEM_API'
let siteId = 1

before(async () => {
  jwtToken = await Token.get()
})

beforeEach(async () => {
  await Database.table('list_placements').delete()
  await Database.table('lists').delete()
  await Database.table('posts').delete()
  await Database.table('positions_scores').delete()
})

test('It can create new listPlacement ', async ({ client, assert }) => {
  let {response, listPlacement} = await ResourceBuilder.ListPlacement(client, jwtToken)
  response.assertStatus(200)
  let recievedListPlacement = response.body.data
  assert.exists(recievedListPlacement.id)
  assert.equal(recievedListPlacement.pageId, listPlacement.pageId)
})

test('It cannot create new listPlacement with dummy user ', async ({ client, assert }) => {
  let localJwtToken = await Token.getNew('DUMY_USER_EMAIL')
  let {response, listPlacement} = await ResourceBuilder.ListPlacement(client, localJwtToken, null, true )
  response.assertStatus(403)
})

test('It can create new listPlacement with system api user ', async ({ client, assert }) => {
  let localJwtToken = await Token.getNew(API_USER)
  let {response, listPlacement} = await ResourceBuilder.ListPlacement(client, localJwtToken)
  response.assertStatus(200)
})

test('It can update listPlacement ', async ({ client, assert }) => {
  let {response, listPlacement} = await ResourceBuilder.ListPlacement(client, jwtToken)
  response.assertStatus(200)

  let newListId = 99
  listPlacement.listId = newListId
  let update = await ResourceBuilder.ListPlacement(client, jwtToken, listPlacement)
  update.response.assertStatus(200)

  let recievedListPlacement = update.response.body.data
  assert.equal(recievedListPlacement.listId, newListId)
})

test('It can get list with API_USER', async ({ client, assert }) => {
  let {response, list} = await ResourceBuilder.List(client, jwtToken)
  let createdList = response.body.data
  let lp = {
    listId: createdList.id
  }

  let localJwtToken = await Token.getNew(API_USER)

  let created = await ResourceBuilder.ListPlacement(client, localJwtToken, lp)

  let getListUrl = UrlBuilder.listPlacement(1, 'get-list-from-params')
  response = await client
    .get(getListUrl)
    .header('jwt_token', localJwtToken)
    .send(created.listPlacement)
    .end()

  DisplayResponseError(response)

  response.assertStatus(200)
})

test('It can get list of type review from listPlacement ', async ({ client, assert }) => {
  let listObject = {
    type: 'review'
  }
  let {response, list} = await ResourceBuilder.List(client, jwtToken, listObject)
  let createdList = response.body.data
  let lp = {
    listId: createdList.id
  }
  let created = await ResourceBuilder.ListPlacement(client, jwtToken, lp)

  let getListUrl = UrlBuilder.listPlacement(1, 'get-list-from-params')
  response = await client
    .get(getListUrl)
    .header('jwt_token', jwtToken)
    .send(created.listPlacement)
    .end()

  DisplayResponseError(response)

  response.assertStatus(200)

  let recievedList = response.body.data 
  assert.exists(recievedList.type)
  assert.exists(recievedList.desktop)
  assert.exists(recievedList.mobile)
  assert.equal(recievedList.isMobileSpecific, list.isMobileSpecific)
  assert.equal(recievedList.name, list.name)
  assert.equal(recievedList.description, list.description)
  let brands = recievedList.desktop.brands
  assert.equal(brands.length, list.desktop.brands.length)
  assert.equal(brands[0].id, list.desktop.brands[0].id)
  assert.exists(brands[0].name)
  assert.notExists(brands[0].score)
  assert.notExists(brands[0].scoreText)
  assert.notExists(brands[0].stars)
})

test('It can get list of type manual from listPlacement ', async ({ client, assert }) => {
  let listObject = {
    type: 'manual'
  }
  let {response, list} = await ResourceBuilder.List(client, jwtToken, listObject)
  let createdList = response.body.data
  let lp = {
    listId: createdList.id
  }
  let created = await ResourceBuilder.ListPlacement(client, jwtToken, lp)

  let getListUrl = UrlBuilder.listPlacement(1, 'get-list-from-params')
  response = await client
    .get(getListUrl)
    .header('jwt_token', jwtToken)
    .send(created.listPlacement)
    .end()

  DisplayResponseError(response)

  response.assertStatus(200)

  let recievedList = response.body.data 
  assert.exists(recievedList.type)
  assert.exists(recievedList.desktop)
  assert.exists(recievedList.mobile)
  assert.equal(recievedList.isMobileSpecific, list.isMobileSpecific)
  assert.equal(recievedList.name, list.name)
  assert.equal(recievedList.description, list.description)
  let brands = recievedList.desktop.brands
  assert.equal(brands.length, list.desktop.brands.length)
  assert.equal(brands[0].id, list.desktop.brands[0].id)
  assert.exists(brands[0].name)
  assert.equal(brands[0].score, list.desktop.brands[0].score)
  assert.equal(brands[0].scoreText, list.desktop.brands[0].scoreText)
  assert.equal(brands[0].stars, list.desktop.brands[0].stars)
})

test('It can get list of type position from listPlacement ', async ({ client, assert }) => {
  

  let listObject = {
    type: 'position'
  }
  let {response, list} = await ResourceBuilder.List(client, jwtToken, listObject)
  let createdList = response.body.data
  let lp = {
    listId: createdList.id
  }

  let ps = await ResourceBuilder.PositionsScore(client, jwtToken, siteId, createdList.contentTypeId)
  ps.response.assertStatus(200)
  assert.equal(ps.PositionsScore.length, 3)

  let created = await ResourceBuilder.ListPlacement(client, jwtToken, lp)

  let getListUrl = UrlBuilder.listPlacement(1, 'get-list-from-params')
  response = await client
    .get(getListUrl)
    .header('jwt_token', jwtToken)
    .send(created.listPlacement)
    .end()

  DisplayResponseError(response)

  response.assertStatus(200)

  let recievedList = response.body.data 
  assert.exists(recievedList.type)
  assert.exists(recievedList.desktop)
  assert.exists(recievedList.mobile)
  assert.equal(recievedList.isMobileSpecific, list.isMobileSpecific)
  assert.equal(recievedList.name, list.name)
  assert.equal(recievedList.description, list.description)
  let brands = recievedList.desktop.brands
  assert.equal(brands.length, list.desktop.brands.length)
  assert.equal(brands[0].id, list.desktop.brands[0].id)
  assert.exists(brands[0].name)
  assert.equal(brands[0].score, ps.PositionsScore[0].score)
  assert.equal(brands[0].scoreText, ps.PositionsScore[0].scoreText)
  assert.equal(brands[0].stars, ps.PositionsScore[0].stars)
})

