'use strict'

const {test, beforeEach} = use('Test/Suite')('Post')
const Post = use('App/Models/Post')
const PostRepo = make('App/Repositories/Post')
const ListRepo = make('App/Repositories/List')
const Database = use('Database')


beforeEach (async () => {
   await Database
    .table('lists')
    .delete()

   await Database
    .table('posts')
    .delete()
})

test('save and fetch posts without data for list', async ({ assert }) => {
  //clone the object and change its props
  let listObject = Object.assign({}, listModelData)
  listObject.type = listModelData.type = 'position'
  let isMobile = listObject.isMobileSpecific = false

  // create new list model
  const List = use('App/Models/List')
  let listModel = new List()
  listModel.fill(listObject)
  await listModel.save()

  //add post without data
  listObject = Object.assign({}, listModelData, listPostsTemplate)
  listObject.desktop.brands = postsWithOutData

  await PostRepo.saveForList(listModel, listObject)
  let postsList = await PostRepo.getByListId(listModel.id, isMobile)

  assert.equal(postsList.length, postsWithOutData.length)
  assert.equal(postsList[0].position, 1)
})

test('save and fetch posts with data for list', async ({ assert }) => {
  //clone the object and change its props
  let listObject = Object.assign({}, listModelData)
  listObject.type = listModelData.type = 'manual'
  let isMobile = listObject.isMobileSpecific = false

  // create new list model
  const List = use('App/Models/List')
  let listModel = new List()
  listModel.fill(listObject)
  await listModel.save()

  //add post without data
  listObject = Object.assign({}, listModelData, listPostsTemplate)
  listObject.desktop.brands = postsWithData

  await PostRepo.saveForList(listModel, listObject)
  let postsList = await PostRepo.getByListId(listModel.id, isMobile)

  assert.equal(postsList.length, postsWithData.length)
  assert.equal(postsList[0].position, 1)
  assert.equal(postsList[0].score, postsWithData[0].score)
  assert.equal(postsList[0].stars, postsWithData[0].stars)
  assert.equal(postsList[0].scoreText, postsWithData[0].scoreText)

  //save it again to make sure we get
  await PostRepo.saveForList(listModel, listObject)
  postsList = await PostRepo.getByListId(listModel.id, isMobile)

  assert.equal(postsList.length, postsWithData.length)

  ListRepo.setSiteId(listObject.siteId)
  let list = await ListRepo.getWithBrands(listModel.id)
})

let postsWithOutData = [
  {
    id: 2
  },
  {
    id: 1
  },
  {
    id: 3
  }
]

let postsWithData = [
  {
    id: 141,
    name: "Casino 1",
    ribbon: "ribbon 2",
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  },
  {
    id: 139,
    name: "Casino 1",
    ribbon: "ribbon 2",
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  },
  {
    id: 137,
    name: "Casino 1",
    ribbon: "ribbon 2",
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  }
]

let listModelData = {
  siteId: 1, 
  name: "aaaa", 
  description: 'some description',
  contentTypeId: 1, 
  type: "manual",
  isMobileSpecific: true,
}

let listPostsTemplate = {
  desktop: {
    brands: [],
    notUsedBrands: []
  },
  mobile: {
    brands: [],
    notUsedBrands: []
  }
}

