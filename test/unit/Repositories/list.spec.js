'use strict'

const {test, beforeEach} = use('Test/Suite')('Post')
const Post = use('App/Models/Post')
const PostRepo = make('App/Repositories/Post')
const ListRepo = make('App/Repositories/List')
const Database = use('Database')

beforeEach (async () => {
   await Database
    .table('lists')
    .delete()

   await Database
    .table('posts')
    .delete()

   await Database
    .table('temp_disable_brands')
    .delete()
})

test('can create position list with posts and brands', async ({ assert }) => {
  listObject.type = 'position'
  ListRepo.setSiteId(listObject.siteId)
  let list = await ListRepo.store(listObject)
  list = list.toJSON()
  let list2 = await ListRepo.getWithBrands(list.id)
  list2 = list2.toJSON()

  assert.equal(listObject.desktop.brands.length, list2.desktop.brands.length)
  assert.equal(listObject.mobile.brands.length, list2.mobile.brands.length)
})

test('can create manual list with posts and brands', async ({ assert }) => {
  listObject.type = 'manual'
  ListRepo.setSiteId(listObject.siteId)
  let list = await ListRepo.store(listObject)
  list = list.toJSON()
  let list2 = await ListRepo.getWithBrands(list.id)
  list2 = list2.toJSON()

  assert.equal(listObject.desktop.length, list2.desktop.length)
  assert.equal(listObject.desktop.brands[0].score, list2.desktop.brands[0].score)

  assert.equal(listObject.mobile.length, list2.mobile.length)
  assert.equal(listObject.mobile.brands[0].score, list2.mobile.brands[0].score)
})

test('###### it can disable brand from list', async ({ assert }) => {
  listObject.type = 'position'
  ListRepo.setSiteId(listObject.siteId)
  let list = await ListRepo.store(listObject)
  list = list.toJSON()

  // add brand to discard
  let brandToDisable = listObject.desktop.brands[0].id
  const TempDisableBrand = use('App/Models/TempDisableBrand')
  let tdb = new TempDisableBrand();
  tdb.siteId = listObject.siteId
  tdb.extPostId = brandToDisable
  await tdb.save()
  let list2 = await ListRepo.getForSite(list.id)
  list2 = list2.toJSON()

  assert.equal(listObject.desktop.brands.length - 1, list2.desktop.brands.length)
  assert.equal(listObject.mobile.brands.length - 1, list2.mobile.brands.length)
})

let postsToSave = [
  {
    id: 949,
    name: "Casino 1",
    ribbon: "ribbon 1",
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  },
  {
    id: 870,
    name: "Casino 1",
    ribbon: "ribbon 1",
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  },
  {
    id: 139,
    name: "Casino 2",
    ribbon: "ribbon 2",
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  },
  {
    id: 137,
    name: "Casino 3",
    ribbon: "ribbon 2",
    score: 9.6,
    scoreText: "some text",
    stars: 4.5
  }
]

let listObject = {
  siteId: 1, 
  name: "aaaa", 
  description: 'some description',
  contentTypeId: 1, 
  type: "manual", 
  isMobileSpecific: true, 
  desktop: {
    brands: postsToSave,
    notUsedBrands: []
  },
  mobile: {
    brands: postsToSave,
    notUsedBrands: []
  }
}
