'use strict'

const {test, beforeEach} = use('Test/Suite')('List Placement')
const Post = use('App/Models/Post')
const PostRepo = make('App/Repositories/Post')
const ListRepo = make('App/Repositories/List')
const ListPlacementRepo = make('App/Repositories/ListPlacement')
const Database = use('Database')


beforeEach (async () => {
  await Database.table('list_placements').delete()

})

let listPlacment1 = {
  siteId: 1,
  contentTypeId: 1,
  pageId: 1,
  listId: 1,
  extra: 'some-text'
}

test('it can create list placement', async ({ assert }) => {
  let lp = await ListPlacementRepo.save(listPlacment1)
  assert.equal(lp.pageId, listPlacment1.pageId)
})

