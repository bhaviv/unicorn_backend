'use strict'

const {test, beforeEach} = use('Test/Suite')('External')
const ExternalRepo = make('App/Repositories/External')
const Database = use('Database')


beforeEach (async () => {
})

test('get brands for a given content type', async ({ assert }) => {
  let brands = await ExternalRepo.getBrandsByContentType(3)
  assert.isAbove(brands.length, 5)
})


