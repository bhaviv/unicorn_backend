let actionTypes = [
  {
    type: 'WeightRotation',
    name: 'Weight Rotation'
  },
  {
    type: 'ManualOrder',
    name: 'Manual Order'
  }
]

let ruleTypes = [
  {
    type: 'QueryParam',
    name: 'Query Param'
  },
  {
    type: 'CookieParam',
    name: 'Cookie Param'
  }
]

let contentTypes = [
  {
    slug: 'casino',
    name: 'Casino'
  },
  {
    slug: 'slots',
    name: 'Slots'
  },
  {
    slug: 'betting',
    name: 'Betting'
  },
  {
    slug: 'bingo',
    name: 'Bingo'
  }
]

let ribbons = [
  {
    name: 'Ribbons 1'
  },
  {
    name: 'Ribbons 2'
  },
  {
    name: 'Ribbons 3'
  },
  {
    name: 'Ribbons 4'
  },
]

let sites = [
  { 
    name: 'Playright',
    description: 'New wordpress site for Playright',
    RELATIONS: [
      {
        relation: {
          primary: 'id',
          foreignKey: 'siteId'
        },
        data: {
          'App/Models/ContentType': contentTypes
        }
      },
      {
        relation: {
          primary: 'id',
          foreignKey: 'siteId'
        },
        data: {
          'App/Models/Ribbon': ribbons 
        }
      }
    ]
  }
]

module.exports = {
  'App/Models/ActionType' : actionTypes,
  'App/Models/RuleType' : ruleTypes,
  'App/Models/Site': sites
}
