let contentTypes = [
  {
    slug: 'casino',
    name: 'Casino'
  },
  {
    slug: 'slots',
    name: 'Slots'
  },
  {
    slug: 'betting',
    name: 'Betting'
  },
  {
    slug: 'bingo',
    name: 'Bingo'
  }
]

let ribbons = [
  {
    name: 'Ribbons 1'
  },
  {
    name: 'Ribbons 2'
  },
  {
    name: 'Ribbons 3'
  },
  {
    name: 'Ribbons 4'
  }
]

let brands1 = [
  {
    score: 9.5,
    stars: 4.5,
    ribbonId: 1,
    scoreText: 'Something good'
  },
  {
    score: 9.5,
    stars: 4.5,
    ribbonId: 1,
    scoreText: 'Something Very good'
  },
  {
    score: 9.5,
    stars: 4.5,
    ribbonId: 1,
    scoreText: 'Something Very good'
  }

]

let posts1 = [
  {
    postId: 1,
    order: 1,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 2,
    order: 2,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 3,
    order: 3,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 4,
    order: 4,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 5,
    order: 5,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 6,
    order: 6,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 7,
    order: 7,
    isMobile: 0,
    brandDataId: null
  }
]

let posts2 = [
  {
    postId: 1,
    order: 1,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 2,
    order: 2,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 3,
    order: 3,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 4,
    order: 4,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 5,
    order: 5,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 6,
    order: 6,
    isMobile: 0,
    brandDataId: null
  },
  {
    postId: 7,
    order: 7,
    isMobile: 0,
    brandDataId: null
  }
]

let lists = [
  {
    contentTypeId: 1,
    name: 'List 1',
    description: 'List 1 description',
    type: 'position',
    isMobileSpecific: 0,
    RELATIONS: [
      {
        relation: {
          primary: 'id',
          foreignKey: 'listId'
        },
        data: {
          'App/Models/Post': posts1
        }
      }
    ]
  },
  {
    contentTypeId: 2,
    name: 'List 2',
    description: 'List 2 description',
    type: 'review',
    isMobileSpecific: 0,
    RELATIONS: [
      {
        relation: {
          primary: 'id',
          foreignKey: 'listId'
        },
        data: {
          'App/Models/Post': posts1
        }
      }
    ]
  },
  {
    contentTypeId: 3,
    name: 'List 3',
    description: 'List 3 description',
    type: 'manual',
    isMobileSpecific: 0,
    RELATIONS: [
      {
        relation: {
          primary: 'id',
          foreignKey: 'listId'
        },
        data: {
          'App/Models/Post': posts2
        }
      }
    ]
  }
]


let sites = [
  { 
    name: 'Demo Sites',
    description: 'This is deom sites Playright',
    RELATIONS: [
      {
        relation: {
          primary: 'id',
          foreignKey: 'siteId'
        },
        data: {
          'App/Models/ContentType': contentTypes
        }
      },
      {
        relation: {
          primary: 'id',
          foreignKey: 'siteId'
        },
        data: {
          'App/Models/Ribbon': ribbons 
        }
      },
      {
        relation: {
          primary: 'id',
          foreignKey: 'siteId'
        },
        data: {
          'App/Models/List': lists
        }
      }
    ]
  }
]

module.exports = {
  'App/Models/Site': sites
}
