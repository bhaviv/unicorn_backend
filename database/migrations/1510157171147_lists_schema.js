'use strict'

const Schema = use('Schema')

class ListsSchema extends Schema {
  up () {
    this.create('lists', (table) => {
      table.increments()
      table.integer('siteId')
      table.integer('contentTypeId')
      table.string('name')
      table.string('description')
      table.enum('type',['position', 'review', 'manual'])
      table.boolean('isMobileSpecific').default(false)
    })
  }

  down () {
    this.drop('lists')
  }
}

module.exports = ListsSchema
