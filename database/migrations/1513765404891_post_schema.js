'use strict'

const Schema = use('Schema')

class PostSchema extends Schema {
  up () {
    this.create('posts', (table) => {
      table.increments()
      table.integer('listId')
      table.integer('extPostId')
      table.integer('position')
      table.boolean('isMobile')
      table.float('score')
      table.float('stars')
      table.integer('ribbonId').unsigned().references('id').inTable('ribbons')
      table.string('scoreText', 1024)
    })
  }

  down () {
    this.drop('posts')
  }
}

module.exports = PostSchema
