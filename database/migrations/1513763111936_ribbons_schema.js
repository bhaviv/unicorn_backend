'use strict'

const Schema = use('Schema')

class RibbonsSchema extends Schema {
  up () {
    this.create('ribbons', (table) => {
      table.increments()
      table.integer('siteId').unsigned().references('id').inTable('sites')
      table.string('name')
    })
  }

  down () {
    this.drop('ribbons')
  }
}

module.exports = RibbonsSchema
