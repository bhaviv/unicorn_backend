'use strict'

const Schema = use('Schema')

class ContentTypeSchema extends Schema {
  up () {
    this.create('content_types', (table) => {
      table.increments()
      table.integer('siteId')
      table.string('slug')
      table.string('name')
    })
  }

  down () {
    this.drop('content_types')
  }
}

module.exports = ContentTypeSchema
