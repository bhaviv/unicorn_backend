'use strict'

const Schema = use('Schema')

class ExperimentSchema extends Schema {
  up () {
    this.create('experiments', (table) => {
      table.increments()
      table.integer('siteId')
      table.integer('listPlacementId')
      table.string('name')
      table.string('description')
      table.string('status')
      table.text('rules')
      table.text('actions')
      table.timestamps()  
    })
  }

  down () {
    this.drop('experiments')
  }
}

module.exports = ExperimentSchema
