'use strict'

const Schema = use('Schema')

class AddCategoryColumnSchema extends Schema {
  up () {
    this.table('lists', (table) => {
      table.string('category').notNullable().default('General')
    })
  }

  down () {
    this.table('lists', (table) => {
      table.dropColumn('category')
    })
  }
}

module.exports = AddCategoryColumnSchema
