'use strict'

const Schema = use('Schema')

class PositionsScoreSchema extends Schema {
  up () {
    this.create('positions_scores', (table) => {
      table.increments()
      table.integer('siteId').unsigned().references('id').inTable('sites')
      table.integer('contentTypeId').unsigned().references('id').inTable('content_types')
      table.integer('position')
      table.float('score')
      table.float('stars')
      table.string('scoreText', 1024)
      table.timestamps()
    })
  }

  down () {
    this.drop('positions_scores')
  }
}

module.exports = PositionsScoreSchema
