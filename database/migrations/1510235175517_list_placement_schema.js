'use strict'

const Schema = use('Schema')

class ListPlacementSchema extends Schema {
  up () {
    this.create('list_placements', (table) => {
      table.increments()
      table.integer('siteId').unsigned()
      table.integer('pageId').unsigned()
      table.string('extra', 64)
      table.integer('listId').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('list_placements')
  }
}

module.exports = ListPlacementSchema
