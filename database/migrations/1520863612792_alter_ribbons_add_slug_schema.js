'use strict'

const Schema = use('Schema')

class AlterRibbonsAddSlugSchema extends Schema {
  up () {
    this.table('ribbons', (table) => {
      table.string('slug').notNullable()
    })
  }

  down () {
    this.table('ribbons', (table) => {
      table.dropColumn('slug')
    })
  }
}

module.exports = AlterRibbonsAddSlugSchema
