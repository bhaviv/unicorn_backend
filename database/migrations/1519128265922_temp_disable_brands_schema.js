'use strict'

const Schema = use('Schema')

class TempDisableBrandsSchema extends Schema {
  up () {
    this.create('temp_disable_brands', (table) => {
      table.increments()

      table.integer('siteId')
        .unsigned()
        .notNullable()

      // wordpress post id
      table.integer('extPostId')
        .notNullable()
        .unique()

       table.timestamp('updated_at').defaultTo(this.fn.now())
       table.timestamp('created_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('temp_disable_brands')
  }
}

module.exports = TempDisableBrandsSchema
