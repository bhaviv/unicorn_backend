'use strict'

const Schema = use('Schema')

class RuleTypeSchema extends Schema {
  up () {
    this.create('rule_types', (table) => {
      table.increments()
      table.string('type')
      table.string('name')
    })
  }

  down () {
    this.drop('rule_types')
  }
}

module.exports = RuleTypeSchema
