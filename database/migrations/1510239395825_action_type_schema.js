'use strict'

const Schema = use('Schema')

class ActionTypeSchema extends Schema {
  up () {
    this.create('action_types', (table) => {
      table.increments()
      table.string('type')
      table.string('name')
    })
  }

  down () {
    this.drop('action_types')
  }
}

module.exports = ActionTypeSchema
