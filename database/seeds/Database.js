'use strict'

/*
|--------------------------------------------------------------------------
| Database Seeder
|--------------------------------------------------------------------------
| Database Seeder can be used to seed dummy data to your application
| database. Here you can make use of Factories to create records.
|
| make use of Ace to generate a new seed
|   ./ace make:seed [name]
|
*/

let models = require('../seedData/models.js')

class DatabaseSeeder {
  async seedModels (modelsData, connection) {
    for(let modelName in modelsData) {
      let modelArray = modelsData[modelName]
      for(let i in modelArray ) {
        let modelData = modelArray[i]
        let model = new (use(modelName))()
        for(let prop in modelData) {
          if (prop === 'RELATIONS') continue
          let value = modelData[prop]
          model[prop] = value
        }
        if (connection) {
          model[connection.foreignKey] = connection.value
        }
        await model.save()
        if (modelData['RELATIONS']) {
          for(let j in modelData['RELATIONS']) {
            let models = modelData['RELATIONS'][j].data
            let relation = modelData['RELATIONS'][j].relation
            let connection = {
              foreignKey: relation.foreignKey,
              value: model[relation.primary]
            }
            this.seedModels(models, connection)
          }
        }
      }
    }
  }

  async run () {
    await this.seedModels(models)
    console.log('finish seeding ctrl-c to exit')
  }
}

module.exports = DatabaseSeeder
