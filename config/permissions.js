'use strict'

let prefix = 'unicorn.'
let Permissions = {
  Experiments: {
    View: prefix + 'experiments.view',
    Edit: prefix + 'experiments.edit',
    Add: prefix + 'experiments.add',
    Destroy: prefix + 'experiments.delete',
    Activate: prefix + 'experiments.activate.edit',
    Stop: prefix + 'experiments.stop.edit',
    Clone: prefix + 'experiments.clone.edit',
  },

  ListPlacement: {
    View: prefix + 'list-placements.view',
    Edit: prefix + 'list-placements.edit',
    Add: prefix + 'list-placements.add',
    Destroy: prefix + 'list-placements.delete',
  },

  List: {
    View: prefix + 'list.view',
    Edit: prefix + 'list.edit',
    Add: prefix + 'list.add',
    Destroy: prefix + 'list.delete',
  },

  ToggleDisableBrand: {
    Add: prefix + 'disable-brand.add'
  }
}

/**
 * Permissions that are allowed for system api (wp calls)
 */
let AllowedSystemApi = Permissions.AllowedSystemApi = {}
AllowedSystemApi[prefix + 'list-placements.view'] = true
AllowedSystemApi[prefix + 'list-placements.add'] = true
AllowedSystemApi[prefix + 'list.view'] = true

module.exports = Permissions
