# Unicorn

Application to build and manage list

## Installing

1. install adonis: `npm i -g @adonisjs/cli`
2. Clone the project: `git clone git@bitbucket.org:trafficpoint/unicorn_backend4.git` 
3. Go to project folder: `cd unicorn_backend4`
4. Run `npm install` to install all dependencies
5. Make a copy of `.env.example` rename it to `.env`
6. Make a copy of `.env` rename it to `.env.testing` 
change name of the database to `db_name_test`
7. Run migration `node ace migration:run `
8. Seed db : `node ace seed`
9. Run migration for test db: `ENV_PATH=.env.testing adonis migration:run`
10. Seed db for test: `ENV_PATH=.env.testing adonis see`

##Running the app
Run `adonis serve --dev` for development or `adonis serve` for production

## Running the tests


run all test

```
node ace test 
```
 run test file
```
node ace test -f path_to_file
```

