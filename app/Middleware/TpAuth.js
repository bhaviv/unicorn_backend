'use strict'
const jwt = require('jsonwebtoken')
const Config = use('Config')
const Permissions = use('Permissions')

class TpAuth {

  async handle ({ request, session }, next) {
    let jwtToken = request.headers().jwt_token
    //Exception if jwtTOken empty
    let secret = Config.get('app.appKey')
    try {
      let userPayload = await this._verifyRequestToken(jwtToken, secret)
      request.userEmail = userPayload.sub
      request.userPermissions = await Permissions.getUserPermissions(request, userPayload.permissions)
    } catch (error) {
      if (error.name == 'TokenExpiredError') {
        throw {message: 'Your token has expired please login again', status: 401}
      } else if (error.name == 'JsonWebTokenError') {
        throw {message: 'Token not exists. please login', status: 401}
      } else {
        console.log('TpAuthError', error)
        throw {message: 'Unknown error', status: 500}
      }
    }
    // yield next to pass the request to next middleware or controller
    return await next()
  }

  /**
   * verifies request JWT token
   *
   * @param  {String} token
   * @return {Promise}
   *
   * @private
   */
  _verifyRequestToken (token, secret) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, {}, (error, decoded) => {
        if (error) {
          return reject(error)
        }

        /**
         * For backward compatibility we are going to auto detect the
         * decoded payload and return it as a new payload when it
         * does not have a uid.
         */
        if (decoded.payload && typeof (decoded.payload) === 'number') {
          decoded.payload = {uid: decoded.payload}
        }
        resolve(decoded)
      })
    })
  }

}

module.exports = TpAuth
