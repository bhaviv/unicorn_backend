'use strict'

const { Command } = require('@adonisjs/ace')
const Config = use('Config')
const UserPermission = use('App/Models/cms/UserPermission')

let permissionTypes = {'view':true , 'add':true, 'edit':true, 'delete':true}

class UpdatePermissions extends Command {
  static get signature () {
    return 'update:permissions'
  }

  static get description () {
    return 'Update permissions name in the CMS roles'
  }

  async handle (args, options) {
    let permissions = Config.get('permissions')
    this.permissions = []
    this.permissionsToArray(permissions)

    for( let p in this.permissions) {
      let per = this.permissions[p]
      await this.createIfNotExists(per)
    }
    this.info('finish update permissions')
  }

  async createIfNotExists(permission) {
    let pArray = permission.split('.')
    let type = pArray.pop()
    let name = pArray.join('.')
    if (!permissionTypes[type]) {
      throw "Type is wrong for: " + permission
    }

    let uPermission = await this.getPermission(name,type)
    if (!uPermission) {
      await this.createPermission(name, type)
      console.log('new permission created. name:', name,' type:',  type)
    }
  }

  async getPermission(name, type) {
    return await UserPermission.query()
      .where('name', name)
      .where('type', type )
      .first()
  }

  async createPermission(name, type) {
      let newUserPermission = new UserPermission()
      newUserPermission.name = name
      newUserPermission.type = type 
      return await newUserPermission.save()
  }


  permissionsToArray (permissions) {
    if (permissions !== null && typeof permissions === 'object') {
      Object.keys(permissions).forEach((key,index) => {
        this.permissionsToArray(permissions[key])
      })
    } else {
      this.permissions.push(permissions)
    }
  }
}

module.exports = UpdatePermissions
