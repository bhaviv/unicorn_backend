'use strict'

const PositionsScoreRepo = make('App/Repositories/PositionsScore')

class PositionsScoreController {

  async show ({params, response}) {
     let contentTypeId = params.contentTypeId
     let siteId = params.siteId
     const PositionsScore = await PositionsScoreRepo.getByContentTypeId(siteId, contentTypeId)
     response.send({ data: PositionsScore.toJSON()})
  }

  async store ({params, request, response}) {
    let siteId = params.siteId
    let contentTypeId = params.contentTypeId
    let PositionsScoreData = request.all()
    let PositionsScore = await PositionsScoreRepo.store(siteId, contentTypeId, PositionsScoreData)
    response.send({ data: PositionsScore.toJSON()})
  }
}

module.exports = PositionsScoreController
