'use strict'

const ExperimentRepo = make('App/Repositories/Experiment')
const Config = use('Config')

class ExperimentController {
  async index ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.View'))
     let siteId = params.siteId
     let lpId = params.lpId
     const experiments = await ExperimentRepo.list(siteId, lpId)
     response.send({ data: experiments.toJSON()})
  }

  async show ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.View'))
     let experimentId = params.id
     const experiment = await ExperimentRepo.find(experimentId)
     response.send({ data: experiment.toJSON()})
  }

  async store ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.Add'))
    let experimentData = request.all()
    experimentData.siteId = params.siteId
    experimentData.listPlacementId = params.lpId
    let experiment = await ExperimentRepo.store(experimentData)
    response.send({ data: experiment.toJSON()})
  }

  async update ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.Edit'))
    let experimentData = request.all()
    experimentData.siteId = params.siteId
    let experiment = await ExperimentRepo.update(experimentData)
    response.send({ data: experiment.toJSON()})
  }

  async destroy ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.Destroy'))
    let experimentId = params.id
    let result = await ExperimentRepo.destroy(experimentId)
    response.send({ data: result})
  }

  async clone ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.Clone'))
    const id = params.id
    const result = await ExperimentRepo.clone(id)
    response.ok({ data: result.toJSON()})
  }

  async activate ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.Activate'))
    const id = params.id
    const result = await ExperimentRepo.activate(id)
    response.ok({ data: result})
  }

  async stop ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.Experiments.Stop'))
    const id = params.id
    const result = await ExperimentRepo.stop(id)
    response.ok({ data: result})
  }

  //TODO NEED TO BE REMOVED 
  async getActive ({params, response}) {
    const siteId = params.siteId
    const listPlacementId = params.lpId
    const result = await ExperimentRepo.getActive(siteId, listPlacementId)
    response.ok({ data: result})
  }
}

module.exports = ExperimentController
