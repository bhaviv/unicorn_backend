'use strict'

const Site = use('App/Models/Site')
const ContentType = use('App/Models/ContentType')
const Ribbon = use('App/Models/Ribbon')
const ExternalRepo = make('App/Repositories/External')
const Config = use('Config')

class SiteController {

  async getSite ({request, response, params}) {
    let id = params.id
    const site = 
      await Site
        .query()
        .where('id', id)
        .first()
    response.send({data: site.toJSON()})
  }

  async getSites ({request, response, params}) {
    let id = params.id 
    const sites = await Site.all()
      
    response.send({data: sites.toJSON()})
  }

  async getBrands ({request, response, params}) {
    let siteId = params.siteId
    let contentTypeId = params.contentTypeId
    let brandList = await ExternalRepo.getBrandsByContentType(contentTypeId)
    response.send({data: brandList})
  }

  async getContentTypes ({request, response, params}) {
    let siteId = params.siteId
    const contentTypes = await ContentType
      .query() 
      .where('siteId', siteId)
      .fetch()
    response.send({data: contentTypes.toJSON()})
  }

  async getRibbons ({request, response, params}) {
    let siteId = params.siteId
    const ribbons = await Ribbon 
      .query() 
      .where('siteId', siteId)
      .fetch()
    response.send({data: ribbons.toJSON()})
  }

  /**
   * Toggle enable/disable brand
   */
  async toggleBrand ({request, response, params}) {
    await request.userPermissions
      .canOrThrow(params.siteId, Config.get('permissions.ToggleDisableBrand.Add'))
    let siteId = params.siteId
    const TempDisableBrand = make('App/Repositories/TempDisableBrand')
    let result = await TempDisableBrand.toggleBrand(params.siteId, params.brandId, params.type)
    response.send({data: result})
  }
}

module.exports = SiteController 
