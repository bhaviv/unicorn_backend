'use strict'

const jwt = require('jsonwebtoken')
const Config = use('Config')
const Env = use('Env')

class ExampleController {

  async loginExample ({request, response, view}) {
    let token = await this._signToken()
    let frontUrl =  Env.get('FRONT_URL', 'http://127.0.0.1:3333')
    return view.render('login-example', { token, frontUrl })
  }

   _signToken () {
    let subject = "barak@trafficpoint.io"
    let secret = Config.get('app.appKey')
    let options = {}

    return new Promise((resolve, reject) => {
      let expirationTime = Math.floor(Date.now() / 1000) + (60 * 60)
      jwt.sign({
        exp: expirationTime,
        sub: subject
       }, secret, options, function (error, token) {
        if (error) {
          return reject(error)
        }
        resolve(token)
      })
    })
  }

  async _verifyRequestToken (token, secret) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, {}, (error, decoded) => {
        if (error) {
          return reject(error)
        }

        /**
         * For backward compatibility we are going to auto detect the
         * decoded payload and return it as a new payload when it
         * does not have a uid.
         */
        if (decoded.payload && typeof (decoded.payload) === 'number') {
          decoded.payload = {uid: decoded.payload}
        }
        resolve(decoded)
      })
    })
  }
}

module.exports = ExampleController
