'use strict'

const ActionType = use('App/Models/ActionType')

class ActionTypeController {
  async index ({response}) {
     const actionTypes = await ActionType.all()
     response.send({ data: actionTypes.toJSON()})
  }
}

module.exports = ActionTypeController
