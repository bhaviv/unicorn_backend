'use strict'

const ListRepo = make('App/Repositories/List')
const Config = use('Config')

class ListController {
  async index ({params, request, response}) {
     await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.List.View'))
     let contentTypeSlug = request.input('contentTypeSlug')
     let lists
     ListRepo.setSiteId(params.siteId)
     if (contentTypeSlug) {
        lists = await ListRepo.getByContentTypeSlug(contentTypeSlug)
     } else {
       lists = await ListRepo.list()
     }
    
     response.send({ data: lists})
  }

  async show ({params, request, response}) {
     await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.List.View'))
     let listId = params.id
     let isForSite = request.input('forSite')
     let list
     ListRepo.setSiteId(params.siteId)
     if (isForSite) {
       list = await ListRepo.getForSite(listId)
     } else {
       list = await ListRepo.getWithBrands(listId)
     }

     response.send({ data: list})
  }

  async store ({params, request, response}) {
     await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.List.Add'))
    let listData = request.all()
    listData.siteId = params.siteId
    let list = await ListRepo.setSiteId(params.siteId).store(listData)
    response.send({ data: list.toJSON()})
  }

  async update ({params, request, response}) {
     await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.List.Edit'))
    let listData = request.all()
    listData.siteId = params.siteId
    ListRepo.setSiteId(params.siteId)
    let list = await ListRepo.update(listData)
    response.send({ data: list.toJSON()})
  }

  async destroy ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.List.Destroy'))
    let listId = params.id
    ListRepo.setSiteId(params.siteId)
    let result = await ListRepo.destroy(listId)
    response.send({ data: result})
  }
}

module.exports = ListController
