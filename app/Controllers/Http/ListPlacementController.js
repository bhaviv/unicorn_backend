'use strict'

const ListPlacementRepo = make('App/Repositories/ListPlacement')
const ListRepo = make('App/Repositories/List')
const Config = use('Config')

class ListPlacementController {
  async getListFromParams ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.ListPlacement.View'))
    let lpParams = request.all()
    lpParams.siteId = params.siteId
    let list = await ListPlacementRepo.getListFromParams(lpParams)
    response.send({ data: list})
  }

  async store ({params, request, response}) {
    await request.userPermissions
      .canOrThrow(params.siteId,Config.get('permissions.ListPlacement.Add'))
    let listPlacementData = request.all()
    listPlacementData.siteId = params.siteId
    let listPlacement = await ListPlacementRepo.save(listPlacementData)
    response.send({ data: listPlacement.toJSON()})
  }
}

module.exports = ListPlacementController
