'use strict'

const RuleType = use('App/Models/RuleType')

class RuleTypeController {
  async index ({response}) {
     const ruleTypes = await RuleType.all()
     response.send({ data: ruleTypes.toJSON()})
  }
}

module.exports = RuleTypeController
