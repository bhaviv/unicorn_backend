'use strict'

const Model = use('Model')

class List extends Model {

  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }

  posts () {
    return this.hasMany('App/Models/Post', 'id', 'listId')
  }
}

module.exports = List
