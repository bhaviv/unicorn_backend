'use strict'

const Model = use('Model')

class ContentType extends Model {
  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }

  site () {
    return this.belongsTo('App/Models/Site', 'siteId', 'id')
  }
}

module.exports = ContentType
