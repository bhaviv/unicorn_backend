let brandsList1 = [
  { 
    'id':24,
    'slug':'dotty',
    'name':'Dotty Bingo',
    'position':0
  },
  { 
    'id':30,
    'slug':'paddy-power-bingo',
    'name':'Paddy Power Bingo',
    'position':1
  },
  { 
    'id':25,
    'slug':'gala-bingo',
    'name':'Gala Bingo',
    'position':2
  },
  { 
    'id':21,
    'slug':'bingo-diamond',
    'name':'Bingo Diamond',
    'position':3
  },
  { 
    'id':19,
    'slug':'888-ladies',
    'name':'888Ladies',
    'position':4
  },
  { 
    'id':383,
    'slug':'dove',
    'name':'Dove Bingo',
    'position':5
  },
  { 
    'id':22,
    'slug':'butlers',
    'name':'Butlers Bingo',
    'position':6
  },
  { 
    'id':32,
    'slug':'sun',
    'name':'Sun Bingo',
    'position':7
  }
]

let brandsList2 = [
  { 
    'id':24,
    'slug':'dotty',
    'name':'Dotty Bingo',
    'position':0
  },
  { 
    'id':30,
    'slug':'paddy-power-bingo',
    'name':'Paddy Power Bingo',
    'position':1
  },
  { 
    'id':25,
    'slug':'gala-bingo',
    'name':'Gala Bingo',
    'position':2
  },
  { 
    'id':21,
    'slug':'bingo-diamond',
    'name':'Bingo Diamond',
    'position':3
  },
  { 
    'id':19,
    'slug':'888-ladies',
    'name':'888Ladies',
    'position':4
  },
  { 
    'id':383,
    'slug':'dove',
    'name':'Dove Bingo',
    'position':5
  },
  { 
    'id':22,
    'slug':'butlers',
    'name':'Butlers Bingo',
    'position':6
  },
  { 
    'id':32,
    'slug':'sun',
    'name':'Sun Bingo',
    'position':7
  }
]

let brandsLists = {
  3: brandsList1,
  8: brandsList2
}

let sitesLists = {
  5: [
    {
      id: 8,
      name: 'Casino Main'
    },
    {
      id: 3,
      name: '888 Main'
    }
  ],

  4: [
    {
      id: 3,
      name: 'Norton Main'
    },
    {
      id: 8,
      name: 'Macafe Main'
    }
  ]
}

module.exports = {
  sitesLists: sitesLists,
  brandsLists: brandsLists
}
