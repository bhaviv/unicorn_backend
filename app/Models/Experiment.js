'use strict'

const Model = use('Model')

class Experiment extends Model {
  getRules (rules) {
    return JSON.parse(rules)
  }

  setRules (rules) {
    return JSON.stringify(rules)
  }

  getActions (actions) {
    return JSON.parse(actions)
  }

  setActions (actions) {
    return JSON.stringify(actions)
  }
}

module.exports = Experiment
