'use strict'

const Model = use('Model')

class Ribbon extends Model {
  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }
}

module.exports = Ribbon
