'use strict'

const Model = use('Model')

class ListPlacement extends Model {
  list () {
    return this.belongsTo('App/Models/List', 'listId', 'id')
  }
}

module.exports = ListPlacement
