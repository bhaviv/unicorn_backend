'use strict'

const Model = use('Model')

class UserPermission extends Model {
 static get connection () {
    return 'cms'
  }

  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }
}

module.exports = UserPermission
