'use strict'

const Model = use('Model')

class Site extends Model {
  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }

  contentTypes () {
    return this.hasMany('App/Models/ContentType', 'id', 'siteId')
  }
}

module.exports = Site
