const { ServiceProvider } = require('@adonisjs/fold')

class PermissionsProvider extends ServiceProvider {
  register () {
    this.app.singleton('Permissions', () => {
      const Config = this.app.use('Adonis/Src/Config')
      const Permissions =  this.app.use('App/Services/Permissions')
      return new Permissions()
    })
  }

  boot () {
    // optionally do some intial setup
  }
}

module.exports = PermissionsProvider
