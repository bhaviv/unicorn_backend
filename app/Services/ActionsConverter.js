
const ListPlacementRepo = make('App/Repositories/ListPlacement')

/**
 * ActionConverter
 * Convert Action format to rule checker format
 */
class ActionConverter {
  async convert(experiment) {
    let actions = []
    for(let action of experiment.actions) {
      let newAction = await this.convertOne(action, experiment.listPlacementId)
      actions.push(newAction)
    }

    return actions
  }
  
  async convertOne (action, listPlacementId) {
    if(action.type === 'WeightRotation') {
      return  await this.weightRotationConverter(action, listPlacementId)
    }
    else if(action.type === 'ManualOrder') {
      return  await this.manualOrderConverter(action, listPlacementId)
    } else {
      return action
    }
  }

  async weightRotationConverter (action, listPlacementId) {
    let selectedBrandIds = action
      .params
      .selectedBrands
      .map(brand => { return brand.id })

    let brandsIds = await this.getBaseLineList(listPlacementId)
    let newAction = {
      type: 'weightRotation',
      brandsBaseLineIds: brandsIds,
      brandsRotateListIds: selectedBrandIds,
      weightMatrix: action.params.matrix
    }

    return newAction
  }

  async manualOrderConverter (action, listPlacementId) {
    let brandsIds = await this.getBaseLineList(listPlacementId)

    let manualOrderBaseLineIds = action.params.orderedList
      .map(brand => { return brand.id })

    let newAction =   {  
      type:"manualOrder",
      displayPercentage: action.params.displayPercentage,
      originalBaseLine: brandsIds,
      manualOrderBaseLineIds: manualOrderBaseLineIds
    }

    return newAction
  }

  async getBaseLineList(listPlacementId) {
    let lp = await ListPlacementRepo.getWithList(listPlacementId)
    let brandsIds = lp.list.brands
      .map(brand => { return brand.id })

    return brandsIds
  }
}

module.exports = ActionConverter
