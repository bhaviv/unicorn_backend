const Config = use('Config')

class SiteApiPermissions {
  constructor () {
    this.permissions = Config.get('permissions.AllowedSystemApi')
  }

  async can(siteId, permissionName) {
    let can = !!this.permissions[permissionName]
    return can
  }

  async canOrThrow(siteId, permissionName) {
    let can = await this.can(siteId,permissionName)
    if (!can) {
      throw {msg: 'Does Not have permission', status: 403}
    }

    return true
  }
}

module.exports = SiteApiPermissions
