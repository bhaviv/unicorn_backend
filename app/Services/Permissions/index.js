class Permissions {
  constructor () {
    // console.log('permission service is started')
  }
  
  getUserPermissions (request, permissions) {
    if (request.userEmail === 'SYSTEM_API') {
      const SiteApiPermissions = use('App/Services/Permissions/SiteApiPermissions')
      return new SiteApiPermissions()
    } else {
      const UserSitePermissions = use('App/Services/Permissions/UserSitePermissions')
      return new UserSitePermissions(permissions)
    }
  }
}

module.exports = Permissions
