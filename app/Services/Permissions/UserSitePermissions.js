class UserSitePermissions {
  constructor (permissions) {
    this._permissions = permissions
  }

  async can(siteId, permissionName) {
    let can = !!(this._permissions[siteId] && this._permissions[siteId][permissionName])

    console.log('can', can, this._permissions)
    return can
  }

  async canOrThrow(siteId, permissionName) {
    let can = await this.can(siteId,permissionName)
    if (!can) {
      throw {msg: 'Does Not have permission', status: 403}
    }

    return true
  }
}

module.exports = UserSitePermissions
