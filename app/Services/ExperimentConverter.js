const ActionsConverter = make('App/Services/ActionsConverter')

/**
 * ExperimentConverter
 * Convert experiment format to rule checker format
 */
class ExperimentConverter {
  async convert (experiment) {
    let result = {
      experimentId: experiment.id,
      siteId: experiment.siteId,
      listIdentifier: experiment.listPlacementId,
      conditionedActions: [
        {
          rules: experiment.rules,
          actions: await ActionsConverter.convert(experiment)
        }
      ]
    }

    return result
  }
}

module.exports = ExperimentConverter
