'use strict'

class TempDisableBrand {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/TempDisableBrand']
  }

  constructor (TempDisableBrand) {
    this.TempDisableBrand = TempDisableBrand 
  }

  async all () {
    return await this.TempDisableBrand.all()
  }

  async find (id) {
    return await this.TempDisableBrand.find(id)
  }

  async getDisabledBrands(siteId) {
    let disabledBrands = await this.TempDisableBrand
      .query()
      .select('extPostId')
      .where('siteId', siteId)
      .fetch()

    disabledBrands =  disabledBrands.toJSON()
    let disabledBrandsObj = {}
    disabledBrands.forEach(brand => disabledBrandsObj[brand.extPostId] = true)
    return disabledBrandsObj
  }

  async toggleBrand (siteId, brandId, type) {
    if (type == 'disable') {
      this.disableBrand(siteId, brandId)
    } else if (type == 'enable') {
      this.enableBrand(siteId, brandId)
    }
  }

  async disableBrand (siteId, brandId) {
    let tdb = new this.TempDisableBrand()
    tdb.siteId = siteId
    tdb.extPostId = brandId 

    return await tdb.save()
  }

  async enableBrand (siteId, brandId) {
    return await this.TempDisableBrand
      .query()
      .where('siteId', siteId)
      .where('extPostId', brandId)
      .delete()
  }
}

module.exports = TempDisableBrand 
