'use strict'

const ExperimentConverter = make('App/Services/ExperimentConverter')

class ExperimentRepository {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/Experiment']
  }

  constructor (Experiment) {
    this.Experiment = Experiment 
  }

  async find (id) {
    return await this.Experiment.find(id)
  }

  async list (siteId, lpId) {
    return await this.Experiment
      .query()
      .where('siteId', siteId)
      .where('listPlacementId', lpId)
      .fetch()
  }

  async store (experiment) {
    let newExperiment = new this.Experiment()
    this._fill(newExperiment, experiment)
    newExperiment.status = 'draft'
    await newExperiment.save()
    const freshInstance = await this.find(newExperiment.id)
    return freshInstance
  }

  async update (experiment) {
    let updateExperiment = await this.find(experiment.id)
    if (!updateExperiment) {
      throw 'Could not find experiment to update with id ' + experiment.id
    }
    this._fill(updateExperiment, experiment)
    await updateExperiment.save()
    const freshInstance = await this.find(experiment.id)
    return freshInstance
  }

  async destroy (id) {
    const experiment = await this.find(id)
    if (!experiment) {
      throw 'Could not find experiment to delete with id ' + experiment.id
    }
    const deleteResult = await experiment.delete()
    const result = {
      res: deleteResult
    }

    return result
  }

  async clone (id) {
    const experiment = await this.find(id)
    let clone = new this.Experiment()
    let experimentData = experiment.toJSON()
    this._fill(clone, experimentData)
    clone.status      = 'draft'
    await clone.save()

    const freshInstance = await this.find(clone.id)
    return freshInstance
  }

  async activate (id) {
    const experiment = await this.find(id)

    if (!experiment || experiment.status !== 'draft') {
      throw new Exceptions.ApplicationException('Only Draft Experiment can be activate', 400)
    }

    // Archive other active experiments if exists
    const experimentToArchive = await this.Experiment
      .query()
      .where('status', 'active')
      .andWhere('siteId', experiment.siteId)
      .andWhere('listPlacementId', experiment.listPlacementId)
      .first()

    if(experimentToArchive) {
      experimentToArchive.status = 'archive'
      await experimentToArchive.save()
    }

    experiment.status      = 'active'
    await experiment.save()

    // TODO CLEAR the list page cache
    const freshInstance = await this.find(experiment.id)
    return freshInstance
  }


  async stop (id) {
    const experiment = await this.find(id)
    if (experiment.status !== 'active') {
      throw new Exceptions.ApplicationException('Only active Experiment can be stopped', 400)
    }
    experiment.status      = 'archive'
    await experiment.save()

    // TODO CLEAR the list page cache
    const freshInstance = await this.find(experiment.id)
    return freshInstance
  }

  async getActive (siteId, listPlacementId) {
    let experiment =  await this.Experiment
      .query()
      .where('siteId', siteId)
      .andWhere('listPlacementId', listPlacementId)
      .andWhere('status', 'active')
      .first()

    if (!experiment) {
      return null
    }

    experiment = experiment.toJSON()
    return await ExperimentConverter.convert(experiment)
  }

  _fill (model, experiment) {
    model.siteId          = experiment.siteId
    model.listPlacementId = experiment.listPlacementId
    model.name            = experiment.name
    model.description     = experiment.description
    model.status          = experiment.status
    model.actions         = experiment.actions
    model.rules           = experiment.rules

    return model
  }
}

module.exports = ExperimentRepository 
