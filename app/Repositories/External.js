'use strict'

const Site = use('App/Models/Site')
const ContentTypeRepo = make('App/Repositories/ContentType')
const Database = use('Database')
const Config = use('Config')
const Env = use('Env')

class ExternalRepository {
  async getBrandsByContentType(contentTypeId) {
    let ct = await ContentTypeRepo.find(contentTypeId)
    let site = await ct.site().first()
    let db_connection_name = 'wp_sites' 
    let tableName = this.getPostsTableName(site)
    let brands = await Database
      .connection(db_connection_name)
      .select('id', 'post_title as name')
      .from(tableName)
      .where('post_type', ct.slug)
      .where('post_status', 'publish')

    return brands
  }

  getPostsTableName(site) {
    return site.id == 1 ? 'funfuncms_posts' : 'funfuncms_' + site.id + '_posts'
  }
}

module.exports = ExternalRepository 
