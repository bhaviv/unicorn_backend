'use strict'

class PositionsScoreRepository {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/PositionsScore']
  }

  constructor (PositionsScore) {
    this.PositionsScore = PositionsScore 
  }

  async find (id) {
    return await this.PositionsScore.find(id)
  }

  async getByContentTypeId (siteId, contentTypeId) {
    return await this.PositionsScore
      .query()
      .where('siteId', siteId)
      .where('contentTypeId', contentTypeId)
      .orderBy('position')
      .fetch()
  }

  async store (siteId, contentTypeId, PositionsScore) {
    await this.destroy(siteId, contentTypeId)
    for(let position in PositionsScore) {
      let ps = PositionsScore[position]
      let newPositionsScore = new this.PositionsScore()
      this._fill(newPositionsScore, ps, siteId, contentTypeId, position)
      await newPositionsScore.save()
    }
    const list  = await this.getByContentTypeId(siteId, contentTypeId)
    return list 
  }

  async update (PositionsScore) {
    let updatePositionsScore = await this.find(PositionsScore.id)
    if (!updatePositionsScore) {
      throw 'Could not find PositionsScore to update with id ' + PositionsScore.id
    }
    updatePositionsScore.fill(PositionsScore)
    await updatePositionsScore.save()
    const freshInstance = await this.getByContentTypeId(PositionsScore.id)
    return freshInstance
  }

  async destroy (siteId, contentTypeId) {
    const deleteResult = await this.PositionsScore
      .query()
      .where('siteId', siteId)
      .where('contentTypeId', contentTypeId)
      .delete()
     
    const result = {
      res: deleteResult
    }

    return result
  }

  _fill (psModel, ps, siteId, contentTypeId, position) {
      psModel.siteId = siteId
      psModel.contentTypeId = contentTypeId
      psModel.position = parseInt(position) + 1
      psModel.score = ps.score
      psModel.stars = ps.stars
      psModel.scoreText = ps.scoreText
  }
}

module.exports = PositionsScoreRepository 
