'use strict'

const _ = require('lodash')

class RibbonRepository {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/Ribbon']
  }

  constructor (Ribbon) {
    this.Ribbon = Ribbon 
  }

  async all () {
    return await this.Ribbon.all()
  }

  async find (id) {
    return await this.Ribbon.find(id)
  }

  async getBySite(siteId) {
    let ribbons = await this.Ribbon
      .query()
      .where('siteId', siteId)
      .fetch()

    return ribbons.toJSON()
  }

  async getBySiteIdAssoc(siteId) {
    let ribbons = await this.getBySite(siteId)
    let assocRibbons = _.keyBy(ribbons, 'id')

    return assocRibbons
  }
}

module.exports = RibbonRepository 
