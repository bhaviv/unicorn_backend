const _ = require('lodash')
const ExternalRepo = make('App/Repositories/External')
const PostRepo = make('App/Repositories/Post')
const RibbonRepo = make('App/Repositories/Ribbon')
const ContentTypeRepo = make('App/Repositories/ContentType')
const PositionsScoreRepo = make('App/Repositories/PositionsScore')

class ListRepository {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/List']
  }

  constructor (List) {
    this.List = List 
  }

  setSiteId (siteId) {
    this.siteId = siteId

    return this
  }

  async getWithBrands (id) {
    let list = await this.List.find(id)
    if(list) {
      list = await this._buildBrandsList(list)
    }

    return list
  }

  /**
   * get list for a site 
   * Return the appropriate data According to the list type 
   * if type=positions return the positions data in the desktop order
   */
  async getForSite (id) {
    let list = await this.getWithBrands(id)
    await this.removeDisabledBrands(list)
    if (list.type === 'position') {
      list = this._addPositionScore(list)
    }

    return list
  }

  /**
   * Remove Disabled Brands
   * Brand could be temporary disabled from all the site lists
   * it will still be shown on the unicorn list
   * @param {Object} list - a list model with brands
   */
  async removeDisabledBrands (list) {
    const TempDisableBrand = make('App/Repositories/TempDisableBrand')
    let disabledBrands = await TempDisableBrand.getDisabledBrands(this.siteId)
    if (disabledBrands.length == 0) return
    list.desktop.brands = list.desktop.brands.filter(brand => !disabledBrands[brand.id])
    if (list.isMobileSpecific) {
      list.mobile.brands = list.mobile.brands.filter(brand => !disabledBrands[brand.id])
    }
  }

  async find (id) {
    let list = await this.List.find(id)

    return list
  }

  async getByContentTypeSlug (contentTypeSlug) {
    let ct = await ContentTypeRepo.getBySlug(contentTypeSlug)
    if (!ct) {
      return []
    }

    let lists = await this.List
    .query()
    .where('siteId', this.siteId)
    .where('contentTypeId', ct.id)
    .fetch()

    return lists.toJSON()
  }

  async list () {
    let lists = await this.List
      .query()
      .where('siteId', this.siteId)
      .fetch()

    return lists.toJSON() 
  }

  async store (list) {
    let newList = new this.List()
    newList = this._fill(newList, list)
    await newList.save()

    await this._saveBrands(newList, list)
    const freshInstance = await this.getWithBrands(newList.id)
    return freshInstance
  }

  async update (list) {
    let updateList = await this.find(list.id)
    if (!updateList) {
      throw 'Could not find list to update with id ' + list.id
    }
    updateList = this._fill(updateList, list)
    await updateList.save()

    await this._saveBrands(updateList, list)

    const freshInstance = await this.getWithBrands(updateList.id)
    return freshInstance
  }

  async destroy (id) {
    const list = await this.find(id)
    if (!list) {
      throw 'Could not find list to delete with id ' + list.id
    }
    const deleteResult = await list.delete()
    const result = {
      res: deleteResult
    }

    return result
  }

  async _saveBrands (listModel, listObject) {
    await PostRepo.saveForList(listModel, listObject)
  }

  _fill (model, list) {
    model.siteId = list.siteId
    model.name = list.name
    model.description = list.description
    model.contentTypeId = list.contentTypeId
    model.type = list.type
    model.category = list.category
    model.isMobileSpecific = list.isMobileSpecific == '1'

    return model
  }

  async _buildBrandsList(list) {
    const TempDisableBrand = make('App/Repositories/TempDisableBrand')
    this.disabledBrands = await TempDisableBrand.getDisabledBrands(this.siteId)
     list['desktop'] =  await this._fillBrands(list)
     list['mobile'] =  await this._fillBrands(list, true)

    return list
  }

  async _fillBrands (list, isMobile) {
    isMobile = !!isMobile
    let isPostHasBrandData = (list.type === 'manual')
    let brandsContainer = {
      brands: [],
      notUsedBrands:[] 
    }

    if (isMobile && !list.isMobileSpecific) {
      // empty container for empty mobile list
      return brandsContainer
    }

    let ribbons = await RibbonRepo.getBySiteIdAssoc(this.siteId)
    let posts = await PostRepo.getByListId(list.id, isMobile)
    let brandList = await ExternalRepo.getBrandsByContentType(list.contentTypeId)

    if (brandList) {
      brandList.forEach(brand => {
        let post = posts.find(p => { 
          return p.extPostId === parseInt(brand.id) 
        })
        
        if (post) {
          let postData = {
            id: post.extPostId,
            name: brand.name,
            ribbonId: post.ribbonId,
            isDisabled: !!this.disabledBrands[brand.id]
          }

          if (post.ribbonId) {
            postData.ribbonName =  ribbons[post.ribbonId] ? ribbons[post.ribbonId].name : null
            postData.ribbonSlug =  ribbons[post.ribbonId] ? ribbons[post.ribbonId].slug : null
          }

          if (isPostHasBrandData) {
            postData['score'] = post.score
            postData['scoreText'] = post.scoreText
            postData['stars'] = post.stars
          }
          brandsContainer.brands[post.position - 1] = postData
        } else {
          brandsContainer.notUsedBrands.push(brand)
        }
      })
    }

    return brandsContainer
  }

  /**
   * Add the position score data to a list
   * @param {Object} list - a list model with brands
   * @param {Object} listPlacement - object with the listPlacement Identifier 
   */ 
  async _addPositionScore (list) {
    let positionsScore = await PositionsScoreRepo.getByContentTypeId(this.siteId, list.contentTypeId)
    positionsScore = positionsScore.toJSON()

    this._addPositionScoreToBrands(list.desktop.brands, positionsScore)
    if(list.isMobileSpecific) {
      this._addPositionScoreToBrands(list.mobile.brands, positionsScore)
    }

    return list
  }

  /**
   * Add the position score data to specific brands list
   * @param {Object} brands - a list of brands
   * @param {Object} positionsScore
   */
  _addPositionScoreToBrands (brands, positionsScore) {
    for (let i in brands) {
      let brand = brands[i]
      if (positionsScore[i]) {
        let ps = positionsScore[i]
        brand.score = ps.score
        brand.scoreText = ps.scoreText
        brand.stars = ps.stars
      }
    }
  }

}

module.exports = ListRepository 
