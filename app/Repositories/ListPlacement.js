'use strict'

const ListRepo = make('App/Repositories/List')

class ListPlacementRepository {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/ListPlacement']
  }

  constructor (ListPlacement) {
    this.ListPlacement = ListPlacement 
  }

  async find (id) {
    return await this.ListPlacement.find(id)
  }

  
  /**
   *
   */
  async getListFromParams (listPlacement) {
    let lp = await this.getFromParams(listPlacement)
    if (!lp) {
      return {
        error: 'placement not found'
      }

    }
    ListRepo.setSiteId(listPlacement.siteId)
    let list = await ListRepo.getForSite(lp.listId)

    return list
  }

  async getFromParams (listPlacement) {
    return await this.ListPlacement
      .query()
      .where('siteId', listPlacement.siteId)
      .where('pageId', listPlacement.pageId)
      .where('extra', listPlacement.extra)
      .first()
  }

  async save (listPlacement) {
    let lp = await this.getFromParams(listPlacement)
    if (lp) {
      if( lp.listId !== listPlacement.listId) {
        lp = this.update(lp, listPlacement)
      }
    } else {
      lp = await this.store(listPlacement)
    }

    return lp 
  }

  async store (listPlacement) {
    let newListPlacement = new this.ListPlacement()
    newListPlacement = this._fill(newListPlacement, listPlacement)
    await newListPlacement.save()
    return newListPlacement 
  }

  async update (lpModel, listPlacement) {
    lpModel.listId = listPlacement.listId
    await lpModel.save()
    return lpModel
  }

  async destroy (id) {
    const listPlacement = await this.find(id)
    if (!listPlacement) {
      throw 'Could not find listPlacement to delete with id ' + listPlacement.id
    }
    const deleteResult = await listPlacement.delete()
    const result = {
      res: deleteResult
    }

    return result
  }

  _fill (model, listPlacement) {
      model.siteId        = listPlacement.siteId
      model.pageId        = listPlacement.pageId
      model.extra         = listPlacement.extra
      model.listId        = listPlacement.listId

      return model
  }
}

module.exports = ListPlacementRepository 
