'use strict'

class PostRepository {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/Post']
  }

  constructor (Post) {
    this.Post = Post 
  }

  /**
   * Get posts array for a list.
   *
   *
   * @method getByListId 
   *
   * @param {Number} listId 
   *
   * @return {Array}
   */
  async getByListId (listId, isMobile) {
    let posts = await this.Post
      .query()
      .where('listId', listId)
      .where('isMobile', isMobile)
      .orderBy('position')
      .fetch()

    return posts.toJSON()
  }

  async saveForList (listModel, listObject) {
    await this.Post
      .query()
      .where('listId', listModel.id)
      .delete()

    let saveBrandData = (listObject.type === 'manual')
    
    await this._savePosts(listModel.id, listObject.desktop.brands, false,  saveBrandData)
    if (listModel.isMobileSpecific) {
      await  this._savePosts(listModel.id, listObject.mobile.brands, true, saveBrandData)
    }
  }

  /**
   * Save post array for a list.
   *
   *
   * @method saveForList 
   *
   * @param  {Number} listId 
   * @param  {Array of Objects} Posts 
   *
   */
  async _savePosts (listId, posts, isMobile, saveBrandData) {
    let postsToSave = []
    if(posts) {
      let position = 1
      for( let i in posts) {
        let postData = posts[i]
        await this._saveOne(postData, listId, position, isMobile, saveBrandData)
        position++
      }
    }
  }
   
  async _saveOne (postData, listId, position, isMobile, saveBrandData) {
    let postObject = {
      listId: listId,
      extPostId: postData.id,
      position: position,
      isMobile: isMobile,
      ribbonId: postData.ribbonId
    }

    if (saveBrandData) {
      postObject['score'] = postData.score
      postObject['scoreText'] = postData.scoreText
      postObject['stars'] = postData.stars
    }

    let post = await this.createOne(postObject)

    return post
  }

  async createOne (postObject) {
    let post =  await this.Post
      .create(postObject)

    return post
  }
}

module.exports = PostRepository 
