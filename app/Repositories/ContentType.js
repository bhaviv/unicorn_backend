'use strict'

class ContentTypeRepository {

  /**
   * injecting required dependencies auto fulfilled
   * by IoC container
   *
   * @return {Array}
   */
  static get inject () {
    return ['App/Models/ContentType']
  }

  constructor (ContentType) {
    this.ContentType = ContentType 
  }

  async all () {
    return await this.ContentType.all()
  }

  async find (id) {
    return await this.ContentType.find(id)
  }

  /**
   * Get  list.
   *
   *
   * @method getBySlug 
   *
   * @param {String} slug
   *
   * @return {Object}
   */
  async getBySlug (slug) {
    return await this.ContentType
      .query()
      .where('slug', slug)
      .first()
  }
}

module.exports = ContentTypeRepository
